package com.amdocs;

import org.junit.Test;
import com.amdocs.Increment;
import static org.junit.Assert.*;

public class IncrementTest {
    
    @Test
    public void testGetCounter_positive() {
        Increment increment = new Increment();
        int result = increment.getCounter();
        assertEquals(1, result);
    }

    @Test
    public void testGetCounter_negative() {
        Increment increment = new Increment();
        int result = increment.getCounter();
        assertNotEquals(3, result);
    }

    @Test
    public void testDecreasecounter_positive() {
        Increment increment = new Increment();
        int result = increment.decreasecounter(0);
        assertEquals(1, result);
    }

    @Test
    public void testDecreasecounter_positiveAgain() {
        Increment increment = new Increment();
        int result = increment.decreasecounter(1);
        assertEquals(1, result);
    }


    @Test
    public void testDecreasecounter_positiveAgainAgain() {
        Increment increment = new Increment();
        int result = increment.decreasecounter(2);
        assertEquals(1, result);
    }


    @Test
    public void testDecreasecounter_negative() {
        Increment increment = new Increment();
        int result = increment.decreasecounter(3);
        assertNotEquals(3, result);
    }



}
