package com.amdocs;

import org.junit.Test;
import com.amdocs.Calculator;
import static org.junit.Assert.*;

public class CalculatorTest {
    
    @Test
    public void testAdd_positive() {
        Calculator calculator = new Calculator();
        int result = calculator.add();
        assertEquals(9, result);
    }

    @Test
    public void testAdd_negative() {
        Calculator calculator = new Calculator();
        int result = calculator.add();
        assertNotEquals(10, result);
    }

    @Test
    public void testSub_positive() {
        Calculator calculator = new Calculator();
        int result = calculator.sub();
        assertEquals(3, result);
    }

    @Test
    public void testSub_negative() {
        Calculator calculator = new Calculator();
        int result = calculator.sub();
        assertNotEquals(9, result);
    }


}
